\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{cashare_documentation}[2021-06-24 v0.0 Document class for Cashare documentation]
\LoadClass[11pt,a4paper,bibliography=totoc,listof=totoc]{report}

\RequirePackage[english]{babel}
\usepackage[T1]{fontenc} % For UTF8 input encoding
\usepackage[utf8]{inputenc} % For UTF8 input encoding
